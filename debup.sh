#!/bin/bash

sudo apt install xorg \ 
			lxlock \
			neofetch \
			i3-wm \
			i3status \
			xterm \
			blueman \
			compton \
			git \
			slim \
			pcmanfm \
			j4-dmenu-desktop \
			suckless-tools \
			firefox \
			irssi \
			mate-calc \
			numix-blue-gtk-theme \
			numix-gtk-theme \
			numix-icon0theme \
			arc-theme \
			pocillo-icon-theme \
			software-properties-common \
			mpv \
			mpg123 \
			figlet \
			system-config-printer \
			lxappearance