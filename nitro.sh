#!/usr/bin/env sh
while true; do
  feh --randomize --bg-fill ${HOME}/.fluxbox/backgrounds/*
  sleep 300
done
